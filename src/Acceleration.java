public class Acceleration {
    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public Acceleration(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
