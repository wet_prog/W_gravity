import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        Fenetre fenetre = new Fenetre();
        fenetre.display.addBalle(new Balle(50,500,5,100,200,0,0,2,0.9f, Color.red,0));
        fenetre.display.addBalle(new Balle(100,50,200,-30,-30,0,0,5,0.95f, Color.black,1));
        fenetre.display.addBalle(new Balle(80,150,300,40,100,0,0,0.5f,0.9f, Color.green, 2));
        fenetre.display.addBalle(new Balle(120,300,100,60,-40,0,0,0.2f,0.9f, Color.orange, 4));
        fenetre.display.addBalle(new Balle(20,200,0,-200,-30,0,0,1,0.9f, Color.blue, 3));
        fenetre.display.addBalle(new Balle(150,400,0,30,0,0,0,1,0.9f, Color.pink, 5));
        fenetre.launch();

        int[] liste = {0,1,2,3,4,5};

        int i = 0;
        while (i < liste.length){
            System.out.println(liste[i]);
            i++;
        }

        for (int num:liste) {
            System.out.println(num);
        }

    }
}
 