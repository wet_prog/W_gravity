public class Vitesse {
    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void add(Acceleration acceleration){
        this.x += acceleration.getX()*Display.step;
        this.y += acceleration.getY()*Display.step;
    }

    public Vitesse(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
