public class Position {
    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void add(Vitesse vitesse, Acceleration acceleration){
        this.x += acceleration.getX()*Math.pow(Display.step,2)+vitesse.getX()*Display.step;
        this.y -= acceleration.getY()*Math.pow(Display.step,2)+vitesse.getY()*Display.step;
    }

    public Position(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
