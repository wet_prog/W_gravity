import java.awt.*;

public class Balle {
    private int size;
    private Position position;
    private Vitesse vitesse;
    private Acceleration acceleration;
    private Color color;
    private float rebond;
    private float friction;
    private int[] ticCount = new int[20];
    public int index;

    public Balle(int size, float Px, float Py, float Vx, float Vy, float Ax, float Ay, float friction, float rebond, Color color, int index) {
        this.size = size;
        this.friction = friction;
        this.rebond = rebond;
        this.position = new Position(Px, Py);
        this.vitesse = new Vitesse(Vx, Vy);
        this.acceleration = new Acceleration(Ax + Fenetre.gravity.getX(), Ay + Fenetre.gravity.getY());
        this.color = color;
        this.index = index;
    }

//    public void display(Graphics g, Display display) {
//        this.nextStep(display);
//        g.setColor(this.getColor());
//        g.fillOval(this.getX(), (int) this.position.getY(), this.size, this.size);
//
//    }

    public void nextStep(Display display) {
        System.out.println(this.vitesse.getX() + " - " + this.vitesse.getY() + " - " + (this.position.getY() ));
        for (int i = 0; i < 20; i++) {
            this.ticCount[i]--;
            if (this.ticCount[i] < 0) {
                this.ticCount[i] = 0;
            }
        }
        if (this.vitesse.getX() < -2 || this.vitesse.getX() < -0.3 ||this.vitesse.getX() > 2 || this.vitesse.getX() > 0.3 || this.position.getY() + this.size < 963) {
            System.out.println("move");
            this.position.add(this.vitesse, this.acceleration);

            if (this.getX() > display.getWidth()) {
                this.setX(1);
            }
            if (this.getX() < 0) {
                this.setX(display.getWidth());
            }
            this.vitesse.add(this.acceleration);
            if (this.position.getY() + 20 + this.size > display.getHeight()) {
                this.position.setY(display.getHeight() - 20 - this.size);
                this.vitesse.setY(-this.vitesse.getY() * this.rebond);
                if (this.vitesse.getX() > 0) {
                    this.vitesse.setX(this.vitesse.getX() >= this.friction ? this.vitesse.getX() - this.friction : 0);
                } else {
                    this.vitesse.setX(this.vitesse.getX() <= this.friction ? this.vitesse.getX() + this.friction : 0);
                }
            }
            for (int i = 0; i < display.balles.length; i++) {
                if (display.balles[i] != null) {


                    if (

                            Math.pow((display.balles[i].position.getX() + display.balles[i].size / 2) - (this.position.getX() + this.size / 2), 2) +
                                    Math.pow((display.balles[i].position.getY() + display.balles[i].size / 2) - (this.position.getY() + this.size / 2), 2)
                                    <= Math.pow(display.balles[i].size / 2 + this.size / 2, 2)
                                    &&
                                    Math.pow((display.balles[i].position.getX() + display.balles[i].size / 2) - (this.position.getX() + this.size / 2), 2) +
                                            Math.pow((display.balles[i].position.getY() + display.balles[i].size / 2) - (this.position.getY() + this.size / 2), 2)
                                            != 0
                            ) {
                        if (this.ticCount[i] == 0 && display.balles[i].ticCount[this.index] == 0) {
                            System.out.println("choc");
                            float tempSpeedX = this.vitesse.getX();
                            float tempSpeedY = this.vitesse.getY();
                            this.vitesse.setX(((this.size - display.balles[i].size) / (this.size + display.balles[i].size) * this.vitesse.getX() + (float) (2 * display.balles[i].size) / (this.size + display.balles[i].size) * display.balles[i].vitesse.getX()) * (this.rebond + display.balles[i].rebond) / 2 - this.vitesse.getX() * this.rebond * 0.01f);
                            display.balles[i].vitesse.setX(((display.balles[i].size - this.size) / (display.balles[i].size + this.size) * display.balles[i].vitesse.getX() + (float) (2 * this.size) / (display.balles[i].size + this.size) * tempSpeedX) * (this.rebond + display.balles[i].rebond) / 2 - display.balles[i].vitesse.getX() * display.balles[i].rebond * 0.010f);
                            this.vitesse.setY(((this.size - display.balles[i].size) / (this.size + display.balles[i].size) * this.vitesse.getX() + (float) (2 * display.balles[i].size) / (this.size + display.balles[i].size) * display.balles[i].vitesse.getX()) * (this.rebond + display.balles[i].rebond) / 2 - this.vitesse.getX() * this.rebond * 0.01f);
                            display.balles[i].vitesse.setY(((display.balles[i].size - this.size) / (display.balles[i].size + this.size) * display.balles[i].vitesse.getX() + (float) (2 * this.size) / (display.balles[i].size + this.size) * tempSpeedY) * (this.rebond + display.balles[i].rebond) / 2 - display.balles[i].vitesse.getX() * display.balles[i].rebond * 0.01f);
                        }
                        this.ticCount[i] = 2;
                        display.balles[i].ticCount[this.index] = 2;
                    }
                }
            }
        }
    }

    public int getX() {
        return (int) Math.floor(this.position.getX());
    }

    public int getY(Display display) {
        return (int) Math.floor(display.getHeight() - this.size / 2 - 20 - this.position.getY());
    }

    public void setX(float x) {
        this.position.setX(x);
    }

    public void setY(float y) {
        this.position.setY(y);
    }

    public Color getColor() {
        return color;
    }

    public int getSize() {
        return size;
    }

    public Position getPosition() {
        return position;
    }

    public Vitesse getVitesse() {
        return vitesse;
    }

    public Acceleration getAcceleration() {
        return acceleration;
    }

    public float getRebond() {
        return rebond;
    }

    public float getFriction() {
        return friction;
    }

    public int[] getTicCount() {
        return ticCount;
    }

    public int getIndex() {
        return index;
    }
}
